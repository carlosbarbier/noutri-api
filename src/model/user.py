from werkzeug.security import check_password_hash, generate_password_hash

from src import db
from src.model.base_model import BaseModel


class User(BaseModel):
    __tablename__ = 'users'

    name = db.Column(db.String(128), nullable=False)
    email = db.Column(db.String(128), nullable=False, unique=True)
    password = db.Column(db.String(255), nullable=False)
    role = db.Column(db.String(15), nullable=False, default='user')
    status = db.Column(db.BOOLEAN, nullable=False, default=False)

    def verify_password(self, pwd):
        return check_password_hash(self.password, pwd)

