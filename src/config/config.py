"""Flask configuration."""
import os

from dotenv import load_dotenv

load_dotenv()


class Config:
    """Base config."""
    SECRET_KEY = os.getenv('SECRET_KEY')
    SESSION_COOKIE_NAME = os.getenv('SESSION_COOKIE_NAME')
    JWT_ERROR_MESSAGE_KEY = 'message'
    JWT_REFRESH_TOKEN_EXPIRES=''


class ProdConfig(Config):
    JWT_ERROR_MESSAGE_KEY = 'message'
    FLASK_ENV = 'production'
    DEBUG = False
    TESTING = False


class DevConfig(Config):
    FLASK_ENV = 'development'
    DEBUG = True
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    SQLALCHEMY_DATABASE_URI = os.getenv('DB_CONNECTION') + '://' + os.getenv('MYSQL_USER') + os.getenv(
        'MYSQL_PASSWORD') + '@' + os.getenv('MYSQL_HOST') + ':' + os.getenv('MYSQL_PORT') + '/' + os.getenv(
        'MYSQL_DB_NAME')
    # SQLALCHEMY_DATABASE_URI='mysql://root@localhost/nouri'
    BUNDLE_ERRORS= True
    JWT_ERROR_MESSAGE_KEY='message'

config = {
    'development': DevConfig,
    'production': ProdConfig,
}
