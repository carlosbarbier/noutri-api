from flask_jwt_extended import get_jwt_identity,get_current_user
from werkzeug.security import generate_password_hash


def non_empty_string(s):
    if not s:
        raise ValueError("Must not be empty string")
    return s


def hash_password(password):
    return generate_password_hash(password)


def verify_sign_user():
    try:
        return get_current_user()
    except RuntimeError as e:
        print(e)
