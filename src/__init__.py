from flask import Flask, jsonify
from flask_jwt_extended import JWTManager
from flask_migrate import Migrate
from flask_sqlalchemy import SQLAlchemy

from src.config import config
from src.routes import register_routes


### Application Factory ###


def create_app():
    app = Flask(__name__)

    # config properties
    app.config.from_object(config.get('development'))

    # database
    db.init_app(app)

    # migration
    migrate.init_app(app, db)
    from src.model import User

    # error
    register_error_handlers(app)

    #jwt
    jwt.init_app(app)

    # routes
    register_routes(app)

    return app


def register_error_handlers(app):
    # 400 - Bad Request
    @app.errorhandler(400)
    def bad_request(e):
        return jsonify({'message': 'data not found'}), 400

        # 403 - Forbidden

    @app.errorhandler(403)
    def forbidden(e):
        return "403"

        # 404 - Page Not Found

    @app.errorhandler(404)
    def page_not_found(e):
        return jsonify({'message': 'route not found'}), 404

        # 405 - Method Not Allowed

    @app.errorhandler(405)
    def method_not_allowed(e):
        return jsonify({'message': 'http method not allow'}), 405

        # 500 - Internal Server Error

    @app.errorhandler(500)
    def server_error(e):
        return jsonify({'message': 'Internal server error'}), 500

    @app.errorhandler(503)
    def server_error(e):
        return jsonify({'message': 'Server unavailable'}), 503

    @app.errorhandler(422)
    def server_error(e):
        return jsonify({'message': 'Invalid data provided'}), 422

    @app.errorhandler(401)
    def server_error(e):
        return jsonify({'message': 'Invalid or expired token'}), 401


db = SQLAlchemy()
migrate = Migrate()
jwt = JWTManager()
