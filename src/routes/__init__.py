from flask_restful import Api

from src.resource import UserRegisterResource,UserLoginResource,UserProtectedResource


def register_routes(app):
    api = Api(app)
    api.add_resource(UserRegisterResource, '/api/users/register')
    api.add_resource(UserLoginResource, '/api/users/login')
    api.add_resource(UserProtectedResource, '/api/users/dashboard')

