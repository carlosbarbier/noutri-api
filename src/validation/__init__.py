from flask_restful import inputs

from src.util import non_empty_string


def user_registration_validator(parser):
    parser.add_argument('name', help='valid name required', required=True, nullable=False, type=non_empty_string,
                        trim=True)
    parser.add_argument('email', help='valid email required',
                        type=inputs.regex(r"(^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$)"), nullable=False)
    parser.add_argument('password', help='valid password required', required=True, nullable=False,
                        type=non_empty_string)


def user_login_validator(parser):
    parser.add_argument('email', help='valid email required',
                        type=inputs.regex(r"(^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$)"), nullable=False)
    parser.add_argument('password', help='valid password required', required=True, nullable=False,
                        type=non_empty_string)
