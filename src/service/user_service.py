from sqlalchemy.exc import IntegrityError
from werkzeug.exceptions import InternalServerError, BadRequest,Unauthorized


class UserService:

    @staticmethod
    def save_user(user):
        try:
            user.save_to_db()
        except IntegrityError as e:
            raise BadRequest("email is taken")
        except Exception as e:
            raise InternalServerError("Internal Server")

    @staticmethod
    def get(self, id):
        pass

    @staticmethod
    def find_user_by_email(email, password):
        from src.deserialiser import UserSchema
        from src.model import User
        user = User.query.filter_by(email=email).first()
        if user is not None and user.verify_password(password):
            schema = UserSchema()
            return schema.dump(user)
        else:
            raise BadRequest("Invalid email or password")


    @staticmethod
    def verify_user(user):
        from src.deserialiser import UserSchema
        from src.model import User
        user = User.query.filter_by(email=email).first()
        if user is not None and user.verify_password(password):
            schema = UserSchema()
            return schema.dump(user)
        else:
            raise BadRequest("Invalid email or password")
