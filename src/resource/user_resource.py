import datetime

from flask import jsonify
from flask_jwt_extended import create_access_token, jwt_required, get_jwt_identity
from flask_restful import Resource, reqparse

from src.service import UserService
from src.util import hash_password
from src.validation import user_registration_validator, user_login_validator

parser = reqparse.RequestParser(bundle_errors=True)


class UserRegisterResource(Resource):
    def post(self):
        from src.model import User
        user_registration_validator(parser)
        args = parser.parse_args()
        user = User(name=args['name'], email=args['email'], password=hash_password(args['password']))
        UserService.save_user(user)

        return {}, 201


class UserLoginResource(Resource):
    def post(self):
        user_login_validator(parser)
        args = parser.parse_args()
        user = UserService.find_user_by_email(args['email'], args['password'])
        expires = datetime.timedelta(days=90)
        access_token = create_access_token(identity=user,expires_delta=expires)
        return jsonify(access_token=access_token)


class UserProtectedResource(Resource):
    @jwt_required()
    def get(self):
        try:
            current_user = get_jwt_identity()
            return current_user, 200
        except Exception as e:
            raise e
